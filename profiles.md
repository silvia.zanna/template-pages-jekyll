---
title: learner profiles
---

## Learner Profiles


### Alex 

- they are *19* years old 
- they just started university where they study archeology 
- they were given a forensic task to characterize a box of “old” bones
- they have a limited timeframe to complete this task
- their goal is to produce a report at the end, which will be read and evaluated by their supervisor who is very strict. Instead of giving instructions on how to tackle the task, the supervisor suggested to look up project management tools 
- Alex goal is to receive a good grade, and leave a lasting impression on their supervisor to get a paid internship. 
- Alex is struggling with their time, has no money, and no instructions on how to tackle task
